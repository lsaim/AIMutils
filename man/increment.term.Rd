\name{increment.term}
\alias{increment.term}
\title{Increment SIS term by one}
\description{
  Returns a vector of SIS terms incremented by one term from a vector of SIS terms.
}
\usage{
  increment.term(Term)
}
\arguments{
  \item{Term}{Character vector containing SIS or DARS terms}
}
\details{
  This function returns the the next SIS term from the one provided.
}
\examples{
  increment.term(c("0992", "0996", "0926", "0944", "1022", "1026", "1154"))
}
