# AIMutils
A set of data utilities in R for UW-Madison administrative data.

Access to this packages is public, and does not require any authentication. To install it run the following:

    devtools::install_gitlab("lsaim/AIMutils", host = "git.doit.wisc.edu")

To use the database connection functions to save database passwords to your keyring and then open UW Madison databases, you should ideally place your database usernames into your .Renviron file by doing the following.

     usethis::edit_r_environ()

This will open up your .Renviron file for editing (and create one if it doesn't exist). This file is hiden (because of the leading ".") and located in your user's Documents folder. Place an entry for all of your database user names (do NOT put your passwords there). Here is a set of example entries.

    INFOACCESS = "DF2"
    DARSP = "DZ7"
    BadgerAnalytics = "bucky.badger@wisc.edu"


# General Computer Setup to use R at UW-Madison


## General Notes
 - For all functions listed below, you should use the campus VPN if you are not using a computer on the UW network.
 - **Restart your computer often**

## Database Connections

- Install Oracle Client 
- [INFOACCESS and DARSP entries to TNSNAMES.ora](https://kb.wisc.edu/msndata/internal/91652)
- [Install Snowflake Driver](https://sfc-repo.snowflakecomputing.com/odbc/win64/latest/index.html)
- Set up ODBC connections using windows ODBC Data Sources (64bit) for [INFOACCESS, DARSP,](https://kb.wisc.edu/msndata/internal/89680) and [Badger Analytics](https://kb.wisc.edu/msndata/internal/120845). Use the following Names so to allow compatibility with password and connection tools in the AIMutils R package.
    * ``INFOACCESS``
    * ``DARSP``
    * ``BadgerAnalytics``

## R 
- [Git version control](https://git-scm.com/download/win): version 2.43
- [R language](https://cran.r-project.org/bin/windows/base/): version 4.3.2
- [R-Studio](https://posit.co/download/rstudio-desktop/): version 2023.12.1
- [R tools](https://cran.r-project.org/bin/windows/Rtools/): version 4.3
- Install common packages used by AIM from CRAN in R terminal: ``install.packages("devtools", "tidyverse", "DBI", "keyring", "odbc", "DT", "ggrepel", "janitor", "quarto")`` **You might need to install these one at a time for inexplicable reasons.**
- Gitlab Personal Access Token for downloading AIM packages in R: 
   * In Gitlab: User &rarr; preferences &rarr;  Personal Access Tokens (with scope API and no expiration).
   * In R terminal: ``usethis::edit_r_environ()``
   * In R environment file that has opened (where this is gitlab PAT): ``GITLAB_PAT = "rhdcw-jLbGwPd82xfIvEfkq9dB"``
- SSH Key for cloning, pulling, and pushing git repositories from gitlab:
   * In R-studio: Tools &rarr; Global Options &rarr; Git/SVN &rarr; Create SSH Key... &rarr; Create
   * Note that if you use a password for the SSH key you will need to enter it every time you use it.
   * From the Git/SVN page you were just on, click "View public key" and copy the text.
   * In Gitlab:  User &rarr; preferences &rarr; SSH Keys. Paste the public key into the "Key" box. Give the key a sensible name (like "Work Desktop"), and an expiration date if you want it to expire.
- Install AIM packages 
   * Utilities: ``devtools::install_gitlab("lsaim/AIMutils", host = "git.doit.wisc.edu")``
   * Queries: ``devtools::install_gitlab("lsaim/AIMqueries", host = "git.doit.wisc.edu")``
- Set up usernames and passwords for use in R using Windows Credential Manager
   * Usernames are set in the renviron, where your GITLAB_PAT is located: ``usethis::edit_r_environ()``. Add the following entries. The name must match the connection names from ODBC Data Sources. They should match this if you followed the instructions above. Put your user names to the right of the `=`, and save the file. 
``` 
INFOACCESS = "DF2"
DARSP = "DZ7"
BadgerAnalytics = "bucky.badger@wisc.edu" 
```
   * INFOACCESS and DARSP Passwords can be stored in the Windows Credential Manager using the AIMutils package functions.  Badger Analytics will open a browser to collect the password when a connection is opened. Enter your password when prompted and it will be securely stored.

```
AIMutils::save.INFOACCESS.password()
AIMutils::save.DARSP.password()
```



